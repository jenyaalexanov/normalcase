using System.Net;
using System.Net.Http.Headers;
using F23.StringSimilarity;
using HtmlAgilityPack;
using Newtonsoft.Json;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapGet("/normalcase/{word}", async (string word) =>
{
    var client = new HttpClient();
    var response = await client.GetAsync($"https://www.google.com/search?q={word}");
    var pageContents = await response.Content.ReadAsStringAsync();
    var pageDocument = new HtmlDocument();
    pageDocument.LoadHtml(pageContents);

    var h3Titles = pageDocument.DocumentNode
        .SelectNodes("//h3")
        .Select(x => x.InnerText)
        .Where(x => !string.IsNullOrEmpty(x))
        .ToList();

    var wordsLength = word.Split(" ", StringSplitOptions.RemoveEmptyEntries).Length;

    var result = new NormalCase();

    var ratcliffObershelp = new RatcliffObershelp();

    foreach (var h3Title in h3Titles)
    {
        var splitedH3Title = h3Title.Split(" ", StringSplitOptions.RemoveEmptyEntries);
        var h3TitleLength = splitedH3Title.Length;
        var currentPosition = 0;

        while (currentPosition < h3TitleLength)
        {
            if (currentPosition + 1 == h3TitleLength)
                currentPosition -= wordsLength - 1;

            var attemptedString = string.Join(" ", splitedH3Title.Skip(currentPosition).Take(wordsLength));

            var similarity = ratcliffObershelp.Similarity(word.ToLower(), attemptedString.ToLower());

            if (similarity == 1.0D)
                return new NormalCase(attemptedString);

            if (similarity > result.Similarity)
            {
                result.Word = attemptedString;
                result.Similarity = similarity;
            }

            currentPosition += wordsLength;
        }
    }

    if (string.IsNullOrEmpty(result.Word))
        throw new Exception("Can't normalize this word");

    return result;
});



// other logic
var items = await File.ReadAllLinesAsync("items.csv");
var apikeys = new List<string>()
{
    //"AIzaSyAlxKTElCRLtMe9lYAgacA1l00WrGjs6ys",
    //"AIzaSyCDTH7Di9c0ifKjbkTtpuFfiFf6M-jLRQw",
    //"AIzaSyAkHLCraAzIro6s3al06VSx7DpLsh1vLDs",
    //"AIzaSyC4G1uJlZdE4RN0urePGsUt4f3nzuPew9Y",
    //"AIzaSyDn0rDgJxMMthixmiF85yA8nV6LuPpJRis",
    //"AIzaSyBX_0kUAmOgfnEj-3HPoA9fEPWHBHMB6Ac",
    //"AIzaSyBxBKTTkwJ9aJsFjM_nEIN0x9IQWAjmgZs",
    //"AIzaSyDZlCoik61M8HZnPpTRp6l3W7F-I97D5iE",
    //"AIzaSyB5Wt13hfduaFX1s2UmjUTuTt4xAYReaXg",
    //"AIzaSyAS8jpQ73wC8-zpOWjKDDJV3u62c0jI7eg",
    "AIzaSyBWspAOEnrEhG_YE8bClVLKKp6CWWcQW6w",
    "AIzaSyB8jsletFZwGRplbsiQeR5tI46LLaUicNI",
    "AIzaSyAFsziCE5nEEVkNI_BNu5jomOMOR3tuNGM",
    "AIzaSyD0sZKM-aV3nf3LNIzIF_LmFZpd0cQuz54",
    "AIzaSyA2SU1fYLqn82QRPVc8w67inoFseZQ9mQo",
    "AIzaSyDRQKnVa9-UX2VtRaOzxjz8xCsqxCPIFKs",
    "AIzaSyDv9wqE9kx1Md6uCPZ5LtIM5mJ40BzBXKY",
    "AIzaSyCNlYjfRS17Kfo6Fpmg7H2g-P6Fj8I5bMs",
    "AIzaSyCSRLGjvVPOAlAGpdHv2b5eWWAnQsh5gN0",
    "AIzaSyAd_iWbN6wyNX_witoymckZdng2GgMfsD0",
    "AIzaSyDBILRDXQPpI4K41qWLDZmfmiPKI0yeuHA",
    "AIzaSyCNfGF56X9-tKgA77ryEUOqK_07WzTy3g0",
    "AIzaSyDb4G917xQm3BRnEQFpBF45N66Wj-tVw0I",
    "AIzaSyBXXi04zpS5rzXt6ZYhVe95TiIZAcsR2-g",
    "AIzaSyDgrhRUYqVhKcoh1J_aTRCOWB5IIkjVBHg",
    "AIzaSyCMt6Q8KbMHeAA50lzx9s5aiIsfghMx_KM",
};

var result = new List<string>
{
    //"0,skill,num1, num2, similarity"
};

try
{
    foreach (var item in items.Skip(1350).ToList())
    {
        var parsed = item.Split(";");
        var word = parsed[1].Replace("'", "");

        while (true)
        {
            try
            {
                var normalCase = await GetNoramalCase(word);
                parsed[1] = @$"'{normalCase.Word}'";
                result.Add($"{string.Join(";", parsed)};{normalCase.Similarity}");
                break;
            }
            catch (Exception)
            {
                apikeys.RemoveAt(0);

                if (apikeys.Count == 0)
                    throw new Exception();
            }
        }
    }
}
catch (Exception)
{
    
}


try
{
    await File.AppendAllLinesAsync("items2.csv", result);
}
catch (Exception)
{
    
}


app.Run();

async Task<NormalCase> GetNoramalCase(string word)
{
    //var httpHandler = new HttpClientHandler()
    //{
    //    Proxy = new WebProxy("zproxy.lum-superproxy.io", 22225)
    //    {
    //        Credentials = new NetworkCredential("lum-customer-hl_78f45c93-zone-static", "vfkqar1ty3rf")
    //    },
        
    //};
    var client = new HttpClient();
    //client.DefaultRequestHeaders.Add("User-Agent", @"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.109 Safari/537.36 OPR/84.0.4316.42 (Edition Yx 05)");
        
    //var response = await client.GetAsync($"https://www.google.com/search?q={word}");

    //if (!response.IsSuccessStatusCode)
    //    throw new Exception("");

    var response = await client.GetAsync($"https://www.googleapis.com/customsearch/v1?key={apikeys[0]}&cx=14fb124ef586f4cb5&q={word}");

    if (!response.IsSuccessStatusCode)
        throw new Exception();

    var pageContents = await response.Content.ReadAsStringAsync();
    //var pageDocument = new HtmlDocument();
    //pageDocument.LoadHtml(pageContents);

    //var h3Titles = pageDocument.DocumentNode
    //    .SelectNodes("//h3")
    //    .Select(x => x.InnerText)
    //    .Where(x => !string.IsNullOrEmpty(x))
    //    .ToList();

    if (string.IsNullOrEmpty(pageContents))
        return new NormalCase{ Word = word, Similarity = 0 };

    Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(pageContents);

    if (myDeserializedClass.Items.Count == 0)
        return new NormalCase { Word = word, Similarity = 0 };

    var h3Titles = myDeserializedClass.Items.Select(x => x.Title).ToList();

    var wordsLength = word.Split(" ", StringSplitOptions.RemoveEmptyEntries).Length;

    var result = new NormalCase();

    var ratcliffObershelp = new RatcliffObershelp();

    foreach (var h3Title in h3Titles)
    {
        var splitedH3Title = h3Title.Split(" ", StringSplitOptions.RemoveEmptyEntries);
        var h3TitleLength = splitedH3Title.Length;
        var currentPosition = 0;

        while (currentPosition < h3TitleLength)
        {
            if (currentPosition + 1 == h3TitleLength)
                currentPosition -= wordsLength - 1;

            var attemptedString = string.Join(" ", splitedH3Title.Skip(currentPosition).Take(wordsLength));

            var similarity = ratcliffObershelp.Similarity(word.ToLower(), attemptedString.ToLower());

            if (similarity == 1.0D)
                return new NormalCase(attemptedString);

            if (similarity > result.Similarity)
            {
                result.Word = attemptedString;
                result.Similarity = similarity;
            }

            currentPosition += wordsLength;
        }
    }

    if (string.IsNullOrEmpty(result.Word))
        throw new Exception("Can't normalize this word");

    return result;
}

// end

public class NormalCase
{
    public NormalCase()
    {

    }

    public NormalCase(string word)
    {
        Word = word;
        Similarity = 1.0D;
    }

    public string? Word { get; set; }

    public double Similarity { get; set; }
}

// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
public class Url
{
    [JsonProperty("type")]
    public string Type { get; set; }

    [JsonProperty("template")]
    public string Template { get; set; }
}

public class Request
{
    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("totalResults")]
    public string TotalResults { get; set; }

    [JsonProperty("searchTerms")]
    public string SearchTerms { get; set; }

    [JsonProperty("count")]
    public int Count { get; set; }

    [JsonProperty("startIndex")]
    public int StartIndex { get; set; }

    [JsonProperty("inputEncoding")]
    public string InputEncoding { get; set; }

    [JsonProperty("outputEncoding")]
    public string OutputEncoding { get; set; }

    [JsonProperty("safe")]
    public string Safe { get; set; }

    [JsonProperty("cx")]
    public string Cx { get; set; }
}

public class NextPage
{
    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("totalResults")]
    public string TotalResults { get; set; }

    [JsonProperty("searchTerms")]
    public string SearchTerms { get; set; }

    [JsonProperty("count")]
    public int Count { get; set; }

    [JsonProperty("startIndex")]
    public int StartIndex { get; set; }

    [JsonProperty("inputEncoding")]
    public string InputEncoding { get; set; }

    [JsonProperty("outputEncoding")]
    public string OutputEncoding { get; set; }

    [JsonProperty("safe")]
    public string Safe { get; set; }

    [JsonProperty("cx")]
    public string Cx { get; set; }
}

public class Queries
{
    [JsonProperty("request")]
    public List<Request> Request { get; set; }

    [JsonProperty("nextPage")]
    public List<NextPage> NextPage { get; set; }
}

public class Context
{
    [JsonProperty("title")]
    public string Title { get; set; }
}

public class SearchInformation
{
    [JsonProperty("searchTime")]
    public double SearchTime { get; set; }

    [JsonProperty("formattedSearchTime")]
    public string FormattedSearchTime { get; set; }

    [JsonProperty("totalResults")]
    public string TotalResults { get; set; }

    [JsonProperty("formattedTotalResults")]
    public string FormattedTotalResults { get; set; }
}

public class CseThumbnail
{
    [JsonProperty("src")]
    public string Src { get; set; }

    [JsonProperty("width")]
    public string Width { get; set; }

    [JsonProperty("height")]
    public string Height { get; set; }
}

public class Metatag
{
    [JsonProperty("og:image")]
    public string OgImage { get; set; }

    [JsonProperty("og:type")]
    public string OgType { get; set; }

    [JsonProperty("twitter:card")]
    public string TwitterCard { get; set; }

    [JsonProperty("og:site_name")]
    public string OgSiteName { get; set; }

    [JsonProperty("twitter:site")]
    public string TwitterSite { get; set; }

    [JsonProperty("viewport")]
    public string Viewport { get; set; }

    [JsonProperty("og:title")]
    public string OgTitle { get; set; }

    [JsonProperty("og:locale")]
    public string OgLocale { get; set; }

    [JsonProperty("og:url")]
    public string OgUrl { get; set; }

    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("og:description")]
    public string OgDescription { get; set; }

    [JsonProperty("theme-color")]
    public string ThemeColor { get; set; }

    [JsonProperty("og:image:width")]
    public string OgImageWidth { get; set; }

    [JsonProperty("google-signin-client-id")]
    public string GoogleSigninClientId { get; set; }

    [JsonProperty("og:image:height")]
    public string OgImageHeight { get; set; }

    [JsonProperty("naver-site-verification")]
    public string NaverSiteVerification { get; set; }

    [JsonProperty("google-signin-scope")]
    public string GoogleSigninScope { get; set; }

    [JsonProperty("referrer")]
    public string Referrer { get; set; }

    [JsonProperty("format-detection")]
    public string FormatDetection { get; set; }

    [JsonProperty("twitter:title")]
    public string TwitterTitle { get; set; }

    [JsonProperty("twitter:url")]
    public string TwitterUrl { get; set; }

    [JsonProperty("appstore:developer_url")]
    public string AppstoreDeveloperUrl { get; set; }

    [JsonProperty("twitter:image")]
    public string TwitterImage { get; set; }

    [JsonProperty("appstore:bundle_id")]
    public string AppstoreBundleId { get; set; }

    [JsonProperty("appstore:store_id")]
    public string AppstoreStoreId { get; set; }

    [JsonProperty("apple-mobile-web-app-capable")]
    public string AppleMobileWebAppCapable { get; set; }

    [JsonProperty("twitter:description")]
    public string TwitterDescription { get; set; }

    [JsonProperty("mobile-web-app-capable")]
    public string MobileWebAppCapable { get; set; }
}

public class CseImage
{
    [JsonProperty("src")]
    public string Src { get; set; }
}

public class Offer
{
    [JsonProperty("price")]
    public string Price { get; set; }

    [JsonProperty("url")]
    public string Url { get; set; }
}

public class Pagemap
{
    [JsonProperty("cse_thumbnail")]
    public List<CseThumbnail> CseThumbnail { get; set; }

    [JsonProperty("metatags")]
    public List<Metatag> Metatags { get; set; }

    [JsonProperty("cse_image")]
    public List<CseImage> CseImage { get; set; }

    [JsonProperty("offer")]
    public List<Offer> Offer { get; set; }
}

public class Item
{
    [JsonProperty("kind")]
    public string Kind { get; set; }

    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("htmlTitle")]
    public string HtmlTitle { get; set; }

    [JsonProperty("link")]
    public string Link { get; set; }

    [JsonProperty("displayLink")]
    public string DisplayLink { get; set; }

    [JsonProperty("snippet")]
    public string Snippet { get; set; }

    [JsonProperty("htmlSnippet")]
    public string HtmlSnippet { get; set; }

    [JsonProperty("cacheId")]
    public string CacheId { get; set; }

    [JsonProperty("formattedUrl")]
    public string FormattedUrl { get; set; }

    [JsonProperty("htmlFormattedUrl")]
    public string HtmlFormattedUrl { get; set; }

    [JsonProperty("pagemap")]
    public Pagemap Pagemap { get; set; }
}

public class Root
{
    [JsonProperty("kind")]
    public string Kind { get; set; }

    [JsonProperty("url")]
    public Url Url { get; set; }

    [JsonProperty("queries")]
    public Queries Queries { get; set; }

    [JsonProperty("context")]
    public Context Context { get; set; }

    [JsonProperty("searchInformation")]
    public SearchInformation SearchInformation { get; set; }

    [JsonProperty("items")]
    public List<Item> Items { get; set; }
}

